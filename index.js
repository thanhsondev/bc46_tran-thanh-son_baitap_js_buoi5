function showKetQua() {
   var diemChuan = document.getElementById('diemChuan').value*1;
   var diemMon1 = document.getElementById('diemMon1').value*1;
   var diemMon2 = document.getElementById('diemMon2').value*1;
   var diemMon3 = document.getElementById('diemMon3').value*1;

   console.log(diemChuan, diemMon1, diemMon2, diemMon3);

   var diemUuTienKhuVuc = document.getElementById("diemUuTienKhuVuc").value*1;
   var diemUuTienDoiTuong = document.getElementById("diemUuTienDoiTuong").value*1;
   
   var diemUuTien = diemUuTienKhuVuc + diemUuTienDoiTuong;

   console.log(diemUuTien);

   var diemTong = diemMon1 + diemMon2 + diemMon3 + diemUuTien;
   var ketQua = '';

   if (diemTong >= diemChuan && diemMon1 > 0 && diemMon2 > 0 && diemMon3 > 0 ) {
    ketQua = 'Bạn đã Đậu'; 
   } else {
    ketQua = 'Thật tiếc, bạn đã rớt';
   }
   document.getElementById("ketQuaBai1").innerHTML = ketQua;

}

//<!-- Bài 2: Tính tiền điện  -->

function showKetQua2() {
 var nhapTen = document.getElementById('nhapTen').value ;
 var soKw = document.getElementById('soKw').value * 1;
 var tienDien = 0;
 if (soKw <= 50) {
  tienDien = 500 * soKw;
 } else if( soKw <= 100) {
   tienDien = 50 * 500 + ((soKw - 50 )* 650);
 } else if( soKw <= 150) {
   tienDien = (50 * 500) + (50 * 650) + ((soKw - 50) * 850);
 } else if(soKw <= 200) {
   tienDien = (50 * 500) + (50 * 650) + (50 * 850) + ((soKw - 50) * 1100);
 } else {
   tienDien = (50 * 500) + (50 * 650) + (50 * 850) + (50 * 1100) + ((soKw - 50) * 1300);
 }

 document.getElementById('ketQuaBai2').innerHTML = `Người sử dụng: ${nhapTen} - Số tiền phải đóng: ${tienDien} `;

}

// <!-- Bài 3: Tính thuế thu nhập cá nhân  -->

function showKetQua3() {
  var nhapTenBai3 = document.getElementById('nhapTenBai3').value;
  var thuNhapNam = document.getElementById('thuNhapNam').value * 1;
  var nguoiPhuThuoc = document.getElementById('nguoiPhuThuoc').value * 1;
  
  var tongThueThuNhapCaNhan = 0;
  if(thuNhapNam <= 60000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.05;
  } else if(thuNhapNam <= 120000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.1;
  } else if(thuNhapNam <= 210000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.15;
  } else if(thuNhapNam <= 384000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.2;
  } else if(thuNhapNam <= 624000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.25;
  } else if(thuNhapNam <= 960000000){
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.3;
  } else{
    tongThueThuNhapCaNhan =(thuNhapNam-4000000-(nguoiPhuThuoc*1600000))*0.35;
  }
  document.getElementById('ketQuaBai3').innerHTML=`Tên người đóng: ${nhapTenBai3} - Thuế thu nhập phải trả: ${Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(tongThueThuNhapCaNhan)}`;

}

// <!-- Bài 4: Tính tiền cáp -->

const NHADAN = "nhaDan"
const DOANHNGHIEP = "doanhNghiep"

function phiHoaDon (loaiKhachHang){
   if(loaiKhachHang == NHADAN){
    return 4.5;
   }
   if(loaiKhachHang == DOANHNGHIEP){
    return 15;
   }
}

function phiDichVuCoBan (loaiKhachHang){
  var soKetNoi = document.getElementById('soKetNoi').value*1;
  if(loaiKhachHang == NHADAN){
   return 20.5;
  }
  if(loaiKhachHang == DOANHNGHIEP){
   if(soKetNoi >= 1 && soKetNoi <= 10 ){
    return 75;
   } else{
    return 75 + ((soKetNoi - 10) * 5);
   }
  }
}

function phiThueKenhCaoCap (loaiKhachHang){
  if(loaiKhachHang == NHADAN){
   return 7.5;
  }
  if(loaiKhachHang == DOANHNGHIEP){
   return 50;
  }
}


//Main function
function showKetQua4()  {
  var loaiKhachHang = document.querySelector('input[name="selector"]:checked').value;
  
  var hoaDon = phiHoaDon(loaiKhachHang);
  var dinhVuCoBan = phiDichVuCoBan(loaiKhachHang);
  var thueKenhCaoCap = phiThueKenhCaoCap(loaiKhachHang);

  var maKhachHang = document.getElementById('maKhachHang').value;
  var soKenhCaoCap = document.getElementById('soKenhCaoCap').value*1;
  

  var tongTienTra = 0;

 if(loaiKhachHang == "nhaDan" ){
  tongTienTra = hoaDon + dinhVuCoBan + (soKenhCaoCap * thueKenhCaoCap);
  
 }
 if (loaiKhachHang == "doanhNghiep") {
  tongTienTra = hoaDon + dinhVuCoBan + (soKenhCaoCap * thueKenhCaoCap);
  
 } 

 document.getElementById('ketQuaBai4').innerHTML = `Mã khách hàng: ${maKhachHang} -  Tiền cáp: $${tongTienTra}`;

}